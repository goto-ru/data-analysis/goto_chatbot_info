Введение в машинное обучение
* https://github.com/Yorko/mlcourse_open
* https://www.coursera.org/learn/vvedenie-mashinnoe-obuchenie/

Базовая обработка текстов
* http://ru.cybernetics.wikia.com/wiki/TF-IDF
* http://www.machinelearning.ru/wiki/images/d/d2/IR.pdf
* https://www.slideshare.net/yauhenklimovich/nltk-python-52455595

Word2Vec
* https://habrahabr.ru/post/249215/
* https://github.com/facebookresearch/fastText

Нейронные сети
* https://habrahabr.ru/company/yandex/blog/175917/
* https://habrahabr.ru/company/yandex/blog/314222/

Git
* https://habrahabr.ru/post/125799/
* https://githowto.com/ru

